# Copyright(c) Rinardi B. Sarean
# Install dotnet interactive to jupyter notebook docker image.
# Use 'jupyter/datascience-notebook' docker image as base.
# Use .NET Core 3.1 LTS 

FROM jupyter/datascience-notebook:latest
ARG UBUNTU_VERSION=20.04

LABEL maintainer="Rinardi B. Sarean <rbs1518@gmail.com>"

USER root

ENV DOTNET_CLI_TELEMETRY_OPTOUT=1
ENV DOTNET_TRY_CLI_TELEMETRY_OPTOUT=1

RUN wget https://packages.microsoft.com/config/ubuntu/$UBUNTU_VERSION/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
    && dpkg -i packages-microsoft-prod.deb \
    && apt-get update --yes \
    && apt-get install --yes dotnet-sdk-3.1

USER ${NB_UID}

RUN dotnet --version \
    && dotnet tool install --global Microsoft.dotnet-interactive --version 1.0.155302

ENV PATH="${PATH}:/home/${NB_USER}/.dotnet/tools"

RUN dotnet-interactive jupyter install